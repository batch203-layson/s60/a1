import { useContext, useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";




export default function AdminDashboard() {


    const navigate = useNavigate();

    function addnewproduct() {
        

        navigate("/addproduct");
    }



    /* const addproduct */


    const { user } = useContext(UserContext);

    // Create allProducts state to contain all the courses from the response of our fetc hData
    const [allProducts, setAllProducts] = useState([]);

    // [SECTION] Setting the course to Active/Inactive
    // Making the course inactive
    const archive = (productItemId, productItemName) => {
        console.log(productItemId);
        console.log(productItemName);

        // Using the fetch method to set the isActive property of the course document to false
        fetch(`${process.env.REACT_APP_API_URL}/productitems/archive/${productItemId}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                productItemIsActive: false
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data) {
                    Swal.fire({
                        title: "Archive Successful",
                        icon: "success",
                        text: `${productItemName} is now inactive.`
                    });
                    // To show the update with the specific operation intiated.
                    fetchData();
                }
                else {
                    Swal.fire({
                        title: "Archive unsuccessful",
                        icon: "error",
                        text: "Something went wrong. Please try again later!"
                    });
                }
            })
    }




    // Making the course active
    const unarchive = (productItemId, productItemName) => {
        console.log(productItemId);
        console.log(productItemName);

        // Using the fetch method to set the isActive property of the course document to false
        fetch(`${process.env.REACT_APP_API_URL}/productitems/archive/${productItemId}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                productItemIsActive: true
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data) {
                    Swal.fire({
                        title: "Unarchive Successful",
                        icon: "success",
                        text: `${productItemName} is now active.`
                    });
                    // To show the update with the specific operation intiated.
                    fetchData();
                }
                else {
                    Swal.fire({
                        title: "Unarchive Unsuccessful",
                        icon: "error",
                        text: "Something went wrong. Please try again later!"
                    });
                }
            })
    }


    //fetchData() function to get all the active/inactive courses.
    const fetchData = () => {
        // get all the courses from the database
        fetch(`${process.env.REACT_APP_API_URL}/productitems/all`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setAllProducts(data.map(product => {
                    return (
                        <tr key={product._id}>
                            <td>
                                {product._id}
                            </td>
                            <td>
                                {product.productItemName}
                            </td>
                            <td>
                                {product.productItemDescription}
                            </td>
                            <td>
                                {product.productItemPrice}
                            </td>
                            <td>
                                {product.productItemStocks}
                            </td>
                            <td>
                                {product.productItemIsActive ? "Active" : "Inactive"}
                            </td>
                            <td>

                                {
                                    //conditional rendering on what button should be visible base on the status of the course
                                    (product.productItemIsActive)
                                        ?
                                        <Button variant="danger" size="small" onClick={() =>
                                            archive(product._id, product.productItemName)
                                        }>
                                            Archive
                                        </Button>
                                        :
                                        <>
                                            <Button variant="success" size="small" className="mx-1" onClick={() => unarchive(product._id, product.productItemName)
                                            }>
                                                Unarchive
                                            </Button>
                                            <Button variant="secondary" size="small" className="mx-1">
                                                Edit
                                            </Button>
                                        </>
                                }
                            </td>




                        </tr>
                    )
                }));
            });
    }


    // To fetch all courses in the first render of the page
    useEffect(() => {
        fetchData();
    }, [])

 

    return (


        (UserContext)
            ?
            <>
                {/* Header for the admin dashboard and functionality for create course and show enrollments */}
                <div className="mt-5 mb-3 text-center">
                    <h1>Admin Dashboard</h1>

                    {/* Adding a new course onClick={() => addproduct()
                    }*/}
                    <Button variant="success" className="mx-2" onClick={()=>addnewproduct()}>
                        Add Product
                    </Button>

                    {/* To view all the user enrollments */}
                    <Button variant="secondary" className="mx-2">
                        Retrieve All
                    </Button>



                </div>

                {/* For view all the courses in the database */}
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Product ID</th>
                            <th>Product Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Stocks</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {allProducts}
                    </tbody>
                </Table>

            </>
            :
            <navigate to="/home" />
    )


}